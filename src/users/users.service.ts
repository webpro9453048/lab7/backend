import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userReponsitory: Repository<User>,
  ) { }
  create(createUserDto: CreateUserDto): Promise<User> {
    return this.userReponsitory.save(createUserDto);
  }

  findAll(): Promise<User[]> {
    return this.userReponsitory.find();
  }

  findOne(id: number): Promise<User> {
    return this.userReponsitory.findOneBy({ id: id });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    await this.userReponsitory.update(id, updateUserDto);
    const user = await this.userReponsitory.findOneBy({ id });
    return user;
  }

  async remove(id: number) {
    const deleteUser = await this.userReponsitory.findOneBy({ id });
    return this.userReponsitory.remove(deleteUser);
  }
}
